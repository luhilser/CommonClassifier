action() {
	if [ -e "${CMSSW_BASE}" ]; then
		local origin="$( pwd )"

		# source this script to install the MEM package
		cd $CMSSW_BASE

		# make the MEM install dir
		mkdir -p src/TTH
		cd src/TTH

		if [ -d RecoLikelihoodReconstruction ]; then
			echo "RecoLikelihoodReconstruction dir exists already, doing nothing." 		
		else 		    
			git clone https://gitlab.cern.ch/luhilser/RecoLikelihoodReconstruction.git RecoLikelihoodReconstruction --branch master
		fi
        
		eval `scramv1 runtime -sh` 		    
		ls 		

		cd $origin
	fi
}
action "$@"
