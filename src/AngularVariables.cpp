#include "../interface/AngularVariables.h"
#include <iostream>
using namespace std;

AngularVariables::AngularVariables(){


  // twiki.cern.ch/twiki/bin/view/CMSPublic/SWGuideBTagging#Preliminary_working_or_operating
  // Preliminary working (or operating) points for CSVv2+IVF
    CSVLwp = 0.423; // 10.1716% DUSG mistag efficiency
    CSVMwp = 0.8484; // 1.0623% DUSG mistag efficiency
    CSVTwp = 0.941; // 0.1144% DUSG mistag efficiency

}


AngularVariables::~AngularVariables(){

}

void AngularVariables::ReconstructTopSystem(const TLorentzVector& lepton, const std::vector<TLorentzVector>& jets, const std::vector<double>& csvs, TLorentzVector met) {
    
    double btagCut = CSVMwp;
    
    int nJets = int(jets.size());
    if(nJets>8) nJets=8;
    
    leptop.SetPxPyPzE(0.,0.,0.,0.);
    hadtop.SetPxPyPzE(0.,0.,0.,0.);
    Lepton.SetPxPyPzE(0.,0.,0.,0.);
    hadb.SetPxPyPzE(0.,0.,0.,0.);
    lepb.SetPxPyPzE(0.,0.,0.,0.);
    mET.SetPxPyPzE(0.,0.,0.,0.);
    
    double metPz[2]={0.,0.};
    double chi=999999;
    
    int nBtags = 0;
    for(int i=0;i<nJets;i++){
        if(csvs[i]>btagCut) nBtags++;
    }
    int nUntags = nJets-nBtags;
    
    double lowest_csv = 99.;
    double second_lowest_csv = 999.;
    int ind_lowest_csv = 999;
    int ind_second_lowest_csv = 999;
    
    if( nUntags<2 ){
      for(int i=0;i<nJets;i++){
	if( csvs[i]<lowest_csv ){
	  second_lowest_csv = lowest_csv;
	  ind_second_lowest_csv = ind_lowest_csv;

	  lowest_csv = csvs[i];
	  ind_lowest_csv = i;
	}
	else if( csvs[i]<second_lowest_csv ){
	  second_lowest_csv = csvs[i];
	  ind_second_lowest_csv = i;
	}
      }
    }
    
    ///////////////////////////// Neutrino /////////////////////////////////////////////////////////////////////
    
    double energyLep = lepton.E();
    double a = (W_mass*W_mass/(2.0*energyLep)) + (lepton.Px()*met.Px() + lepton.Py()*met.Py())/energyLep;
    double radical = (2.0*lepton.Pz()*a/energyLep)*(2.0*lepton.Pz()*a/energyLep);
    radical = radical - 4.0*(1.0 - (lepton.Pz()/energyLep)*(lepton.Pz()/energyLep))*(met.Px()*met.Px() + met.Py()*met.Py()- a*a);
    
    bool imaginary = false;

    if (radical < 0.0)
    {
            imaginary=true;
    }
    if(imaginary)
    {
            radical=0;
            /*
            double value=.001;
            while(true)
            {
                    met.SetPxPyPzE(pfmet_px,pfmet_py,0.0,sqrt(pow(pfmet_px,2)+pow(pfmet_py,2))); //neutrino mass 0, pt = sqrt(px^2+py^2)
    //			energyLep = lepton.E();
                    a = (W_mass*W_mass/(2.0*energyLep)) + (lepton.Px()*met.Px() + lepton.Py()*met.Py())/energyLep;
                    radical = (2.0*lepton.Pz()*a/energyLep)*(2.0*lepton.Pz()*a/energyLep);
                    radical = radical - 4.0*(1.0 - (lepton.Pz()/energyLep)*(lepton.Pz()/energyLep))*(met.Px()*met.Px() + met.Py()*met.Py()- a*a);
                    if(radical>=0)
                            break;
                    pfmet_px-=pfmet_px*value;
                    pfmet_py-=pfmet_py*value;
            }*/
    }
    
    metPz[0] = (lepton.Pz()*a/energyLep) + 0.5*sqrt(radical);
    metPz[0] = metPz[0] / (1.0 - (lepton.Pz()/energyLep)*(lepton.Pz()/energyLep));
    metPz[1] = (lepton.Pz()*a/energyLep) - 0.5*sqrt(radical);
    metPz[1] = metPz[1] / (1.0 - (lepton.Pz()/energyLep)*(lepton.Pz()/energyLep));
    
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    double chi_top_lep=10000;
    double chi_top_had=10000;
    double chi_W_had=10000;
    double minChi = 1000000;
    
    
    
    
    /////////////////////////////////////////// Loop over all jets, both Pz, calcaulte chi-square ///////////////////////////////////////////
    
    TLorentzVector metNew;
    for( int ipznu=0; ipznu<2; ipznu++ ){
        metNew.SetXYZM(met.Px(),met.Py(),metPz[ipznu],0.0); //neutrino has mass 0
        //with b-tag info
        std::vector<TLorentzVector> not_b_tagged,b_tagged;
        //fill not_b_tagged and b_tagged
        for( int i=0;i<nJets;i++ ){
        
            if( (csvs[i]>btagCut && i!=ind_second_lowest_csv && i!=ind_lowest_csv) ) b_tagged.push_back(jets[i]);
            else not_b_tagged.push_back(jets[i]);
        
        }
        //first make possible t_lep's with b-tagged jets (includes making W_lep)
        for( int i=0; i<int(b_tagged.size()); i++ ){
            TLorentzVector W_lep=metNew+lepton; //used for histogram drawing only
            TLorentzVector top_lep=metNew+lepton+b_tagged.at(i);
            chi_top_lep=pow((top_lep.M()-top_mass_lep)/sigma_lepTop,2);
            //next make possible W_had's with not b-tagged jets
            for( int j=0; j<int(not_b_tagged.size()); j++ ){
                for( int k=0; k<int(not_b_tagged.size()); k++ ){
                    if( j!=k ){
                    TLorentzVector W_had=not_b_tagged.at(j)+not_b_tagged.at(k);
                    chi_W_had=pow((W_had.M()-W_mass)/sigma_hadW,2);
                    //now make possible top_had's (using the W_had + some b-tagged jet)
                        for( int l=0; l<int(b_tagged.size()); l++ ){
                            if( l!=i ){
                            TLorentzVector top_had=W_had+b_tagged.at(l);
                            chi_top_had=pow((top_had.M()-top_mass_had)/sigma_hadTop,2);
                            chi=chi_top_lep+chi_W_had+chi_top_had;
                            //accept the lowest chi
                                if( chi<minChi ){
                                    minChi=chi;                        
                                    lepb = b_tagged.at(i);
                                    hadb = b_tagged.at(l);
                                    leptop = top_lep;
                                    hadtop = top_had;
                                    Lepton = lepton;
                                    mET = metNew;
                                }
                            }
                        }
                    }
                }
            }
        }
        
    }
    /*if(nJets==6 && nBtags==3) {
        std::cout << "Common Classifier tophad_pt : " << hadtop.Pt() << std::endl;
        std::cout << "Common Classifier toplep_pt : " << leptop.Pt() << std::endl;
        std::cout << "Common Classifier chi2 : " << minChi << std::endl;
        std::cout << "Common Classifier bhad_pt : " << hadb.Pt() << std::endl;
        std::cout << "Common Classifier blep_pt : " << lepb.Pt() << std::endl;
        std::cout << "Common Classifier met_pt : " << met.Pt() << std::endl;
    }*/
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    
}

std::map<TString,double> AngularVariables::GetAngularVariables() {
    
    std::map<TString,double> angular_variables;
    
    angular_variables["cos_theta_l_bhad"]=TMath::Cos((Lepton.Vect()).Angle(hadb.Vect()));
    angular_variables["cos_theta_blep_bhad"]=TMath::Cos((lepb.Vect()).Angle(hadb.Vect()));
    angular_variables["delta_eta_l_bhad"]=TMath::Abs(Lepton.Eta()-hadb.Eta());
    angular_variables["delta_eta_blep_bhad"]=TMath::Abs(lepb.Eta()-hadb.Eta());
    angular_variables["delta_phi_l_bhad"]=TMath::Abs(Lepton.Phi()-hadb.Phi());
    angular_variables["delta_phi_blep_bhad"]=TMath::Abs(lepb.Phi()-hadb.Phi());
    
    return angular_variables;
    
}