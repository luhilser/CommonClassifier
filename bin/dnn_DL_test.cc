/*
 * DNN DL classification test.
 */

#include <iostream>
#include <limits>
#include <sys/time.h>

#include "TTH/CommonClassifier/interface/DNNClassifier.h"

#ifndef EPSILON
#define EPSILON 1.0e-6
#endif

const TLorentzVector makeVector(double pt, double eta, double phi, double mass)
{
    TLorentzVector lv;
    lv.SetPtEtaPhiM(pt, eta, phi, mass);
    return lv;
}

int test_DNN_DL_v1();

int main()
{
    DNNClassifierBase::pyInitialize();
    test_DNN_DL_v1();
    DNNClassifierBase::pyFinalize();
    return 0;
}

int test_DNN_DL_v1()
{
    // set precision of numbers in cout
    std::cout.precision(8);

    // setup the dnn classifier
    DNNClassifier_DL dnn("v1");

    DNNOutput output3;
    DNNOutput output4;

    // model 3j
    {
        std::vector<TLorentzVector> jets = {
            makeVector(201.97742022, 0.480882972479, -2.40896296501, 27.0501843796),
            makeVector(134.090151115, 1.90997588634, 0.838858008385, 14.7230024259),
            makeVector(89.6709804901, 0.644967615604, 1.51123094559, 12.799938407)
        };
        std::vector<double> jetCSVs = { 0.849623322487, 0.997975826263, 0.6818831563 };

        std::vector<TLorentzVector> leptons = {
            makeVector(92.7541938851, 1.27284574509, -2.35786724091, 0.0491626229221),
            makeVector(76.1411985991, 1.53862166405, -0.523424744606, 0.106239633371)
        };
        TLorentzVector met = makeVector(0.0, 0.0, 0.0, 0.0); // not used in this model

        // evaluate
        output3 = dnn.evaluate(jets, jetCSVs, leptons, met);
    }

    // model 4j
    std::vector<TLorentzVector> jets = {
        makeVector(88.2472256543, -0.925376594067, -0.125109180808, 14.5184706478),
        makeVector(86.4939965229, -0.577705264091, -2.22434067726, 8.90897543576),
        makeVector(65.5470662588, -0.102616533637, 1.09994471073, 11.6266326794),
        makeVector(62.6364762638, -0.288352638483, -0.317469775677, 9.80923660181)
    };
    std::vector<double> jetCSVs = { 0.957217872143, 0.99905860424, 0.774999380112, 0.984192013741 };

    std::vector<TLorentzVector> leptons = {
        makeVector(62.1583853075, 0.411500513554, -3.10956311226, 0.105961925237),
        makeVector(38.5490219467, -0.795875012875, 2.25424337387, 0.0111916854482)
    };
    TLorentzVector met = makeVector(0.0, 0.0, 0.0, 0.0); // not used in this model

    // evaluate
    output4 = dnn.evaluate(jets, jetCSVs, leptons, met);

    // some output
    std::vector<DNNOutput> outputs = { output3, output4 };
    for (size_t i = 0; i < outputs.size(); ++i)
    {
        std::cout << "evaluation for model " << (i + 3) << "j:" << std::endl;
        std::cout << "0 - ttH:   " << std::fixed << outputs[i].ttH() << std::endl;
        std::cout << "1 - ttbb:  " << std::fixed << outputs[i].ttbb() << std::endl;
        std::cout << "2 - ttb:   " << std::fixed << outputs[i].ttb() << std::endl;
        std::cout << "3 - tt2b:  " << std::fixed << outputs[i].tt2b() << std::endl;
        std::cout << "4 - ttcc:  " << std::fixed << outputs[i].ttcc() << std::endl;
        std::cout << "5 - ttlf:  " << std::fixed << outputs[i].ttlf() << std::endl;
        std::cout << "6 - other: " << std::fixed << outputs[i].other() << std::endl;
        std::cout << "most probable class: " << outputs[i].mostProbableClass() << std::endl;
        std::cout << std::endl;
    }

    // validate outputs
    std::vector<double> targetOutputs3 = { 0.23395864, 0.11088409, 0.12151342, 0.28036272, 0.16239489, 0.09088623, 0.0};
    std::vector<double> targetOutputs4 = { 0.74778676, 0.20035280, 0.02076245, 0.02245878, 0.00793151, 0.00070764, 0.0};

    for (size_t i = 0; i < targetOutputs3.size(); ++i)
    {
        assert(fabs(targetOutputs3[i] - output3.values[i]) < EPSILON && "The DNN output for 3j events is incorrect");
    }
    for (size_t i = 0; i < targetOutputs4.size(); ++i)
    {
        assert(fabs(targetOutputs4[i] - output4.values[i]) < EPSILON && "The DNN output for 4j events is incorrect");
    }

    // performance test
    std::cout << "performance test:" << std::endl;
    struct timeval tp;
    gettimeofday(&tp, NULL);
    long int t0 = tp.tv_sec * 1000 + tp.tv_usec / 1000;
    size_t n = 10000;
    for (size_t i = 0; i < n; ++i)
    {
        dnn.evaluate(jets, jetCSVs, leptons, met);
    }
    gettimeofday(&tp, NULL);
    long int t1 = tp.tv_sec * 1000 + tp.tv_usec / 1000;
    int diff = t1 - t0;
    std::cout << n << " evalutions took " << (diff / 1000.) << " seconds" << std::endl;
    std::cout << "=> " << (((float)diff) / n) << " ms per evaluation" << std::endl;

    return 0;
}

#undef EPSILON
