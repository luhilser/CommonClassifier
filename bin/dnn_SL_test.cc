/*
 * DNN SL classification test.
 */

#include <iostream>
#include <limits>
#include <sys/time.h>

#include "TTH/CommonClassifier/interface/DNNClassifier.h"

#ifndef EPSILON
#define EPSILON 1.0e-6
#endif

const TLorentzVector makeVector(double pt, double eta, double phi, double mass)
{
    TLorentzVector lv;
    lv.SetPtEtaPhiM(pt, eta, phi, mass);
    return lv;
}

int test_DNN_SL_v2();
int test_DNN_SL_v3();
int test_DNN_SL_v4();

int main()
{
    DNNClassifierBase::pyInitialize();
    test_DNN_SL_v2();
    test_DNN_SL_v3();
    test_DNN_SL_v4();
    DNNClassifierBase::pyFinalize();
    return 0;
}

int test_DNN_SL_v2()
{
    std::cout << "test DNN SL v2" << std::endl;
    std::cout << "----------------------------------------" << std::endl << std::endl;

    // set precision of numbers in cout
    std::cout.precision(8);

    // setup the dnn classifier
    DNNClassifier_SL dnn("v2");
    dnn.csvCut = 0.8;

    DNNOutput output4;
    DNNOutput output5;
    DNNOutput output6;

    // model 4j
    {
        std::vector<TLorentzVector> jets = {
            makeVector(82.193317445, -1.381465673, 0.875596046, 12.354169733),
            makeVector(61.222326911, -0.624968886, 1.280390978, 7.378321245),
            makeVector(49.268722080, -1.523553133, 2.887234688, 10.714914255),
            makeVector(45.137310817, -0.522410452, 2.953480721, 4.287899215)
        };
        std::vector<double> jetCSVs = { 0.951796949, 0.167348146, 0.164827660, 0.804015398 };

        TLorentzVector lepton = makeVector(126.932662964, 0.030971697, -0.758532584, 0.105700001);
        TLorentzVector met = makeVector(49.720878601, 0., -2.168250561, 0.);

        // evaluate
        output4 = dnn.evaluate(jets, jetCSVs, lepton, met);
    }

    // model 5j
    {
        std::vector<TLorentzVector> jets = {
            makeVector(113.789252322, -0.750472307, -1.340483546, 11.388389050),
            makeVector(104.010172609, -1.185562849, -2.004201174, 20.797776564),
            makeVector(65.583533585, -1.123236418, 2.088073730, 12.479995542),
            makeVector(44.232577186, -1.738008976, -1.576105833, 6.927932890),
            makeVector(40.915780832, -2.206727982, 2.069944143, 5.814550891)
        };
        std::vector<double> jetCSVs = {
            0.998467326, 0.996747077, 0.056155164, 0.838712037, -10.000000000
        };

        TLorentzVector lepton = makeVector(96.684181213, -0.904775918, 0.371903986, 0.105700001);
        TLorentzVector met = makeVector(50.490451813, 0.0, 0.888747931, 0.0);

        // evaluate
        output5 = dnn.evaluate(jets, jetCSVs, lepton, met);
    }

    // model 6j
    std::vector<TLorentzVector> jets = {
        makeVector(561.853400685, -1.687604070, 1.248519063, 48.315901270),
        makeVector(317.050318074, -0.592420697, -1.641329408, 41.454653026),
        makeVector(130.993330071, -0.257405132, -2.218888283, 15.829634094),
        makeVector(90.088794331, -0.758062780, -1.199205875, 11.687776745),
        makeVector(57.793076163, 0.163249642, 2.988173246, 9.959981641),
        makeVector(45.080527604, -1.838460445, 1.761856556, 7.07615728)
    };
    std::vector<double> jetCSVs = {
        0.592329562, 0.156740859, 0.998748481, 0.944896400, 0.268956393, 0.109731160
    };

    TLorentzVector lepton = makeVector(45.567291260, -1.122234225, 2.685425282, 0.105700001);
    TLorentzVector met = makeVector(70.089050293, 0.0, -2.230507374, 0.0);

    // evaluate
    output6 = dnn.evaluate(jets, jetCSVs, lepton, met);

    // some output
    std::vector<DNNOutput> outputs = { output4, output5, output6 };
    for (size_t i = 0; i < outputs.size(); ++i)
    {
        std::cout << "evaluation for model " << (i + 4) << "j:" << std::endl;
        std::cout << "0 - ttH:   " << std::fixed << outputs[i].ttH() << std::endl;
        std::cout << "1 - ttbb:  " << std::fixed << outputs[i].ttbb() << std::endl;
        std::cout << "2 - ttb:   " << std::fixed << outputs[i].ttb() << std::endl;
        std::cout << "3 - tt2b:  " << std::fixed << outputs[i].tt2b() << std::endl;
        std::cout << "4 - ttcc:  " << std::fixed << outputs[i].ttcc() << std::endl;
        std::cout << "5 - ttlf:  " << std::fixed << outputs[i].ttlf() << std::endl;
        std::cout << "6 - other: " << std::fixed << outputs[i].other() << std::endl;
        std::cout << "most probable class: " << outputs[i].mostProbableClass() << std::endl;
        std::cout << std::endl;
    }

    //validate outputs
    std::vector<double> targetOutputs4 = {0.13662557, 0.09063863, 0.12108074, 0.07809219, 0.15359062, 0.20219319, 0.21777909};
    std::vector<double> targetOutputs5 = {0.26851401, 0.15041369, 0.12638351, 0.17068394, 0.11537127, 0.08704317, 0.08159041};
    std::vector<double> targetOutputs6 = {0.16316491, 0.08395188, 0.10267062, 0.1429254, 0.14195499, 0.13069865, 0.23463349};

    for (size_t i = 0; i < targetOutputs4.size(); ++i)
    {
        assert(fabs(targetOutputs4[i] - output4.values[i]) < EPSILON && "The DNN output for 4 jet events is incorrect");
    }
    for (size_t i = 0; i < targetOutputs5.size(); ++i)
    {
        assert(fabs(targetOutputs5[i] - output5.values[i]) < EPSILON && "The DNN output for 5 jet events is incorrect");
    }
    for (size_t i = 0; i < targetOutputs6.size(); ++i)
    {
        assert(fabs(targetOutputs6[i] - output6.values[i]) < EPSILON && "The DNN output for 6 jet events is incorrect");
    }

    // performance test
    std::cout << "performance test:" << std::endl;
    struct timeval tp;
    gettimeofday(&tp, NULL);
    long int t0 = tp.tv_sec * 1000 + tp.tv_usec / 1000;
    size_t n = 10000;
    for (size_t i = 0; i < n; ++i)
    {
        dnn.evaluate(jets, jetCSVs, lepton, met);
    }
    gettimeofday(&tp, NULL);
    long int t1 = tp.tv_sec * 1000 + tp.tv_usec / 1000;
    int diff = t1 - t0;
    std::cout << n << " evalutions took " << (diff / 1000.) << " seconds" << std::endl;
    std::cout << "=> " << (((float)diff) / n) << " ms per evaluation" << std::endl;

    std::cout << std::endl << "done" << std::endl;
    std::cout << "----------------------------------------" << std::endl << std::endl;

    return 0;
}

int test_DNN_SL_v3()
{
    std::cout << "test DNN SL v3" << std::endl;
    std::cout << "----------------------------------------" << std::endl << std::endl;

    // set precision of numbers in cout
    std::cout.precision(8);
    // setup the dnn classifier
    DNNClassifier_SL dnn("v3");

    DNNOutput output4;
    DNNOutput output5;
    DNNOutput output6;

    // model 4j
    {
        std::vector<TLorentzVector> jets = {
            makeVector(146.441362433, -2.26055669785, 0.948636651039, 32.552983859),
            makeVector(145.108558422, -2.15591406822, -1.79363799095, 26.6160656924),
            makeVector(75.767236114, -2.31642627716, -0.256882816553, 12.2186031442),
            makeVector(57.2352080971, -1.72093558311, -2.49330496788, 11.7495895914)
        };
        std::vector<double> jetCSVs = { 0.929098367691, 0.956216454506, 0.509055972099, 0.644097924232 };

        TLorentzVector lepton = makeVector(44.306438446, -1.83303356171, 2.86986398697, 0.105700001109);
        TLorentzVector met = makeVector(0., 0., 0., 0.); //dummy, not needed in this model

        // evaluate
        output4 = dnn.evaluate(jets, jetCSVs, lepton, met);
    }

    // model 5j
    {
        std::vector<TLorentzVector> jets = {
            makeVector(136.288560328, 0.0751701146364, -1.32902193069, 21.5676201491),
            makeVector(92.0679429922, 0.564525187016, 2.06455087662, 15.191872329),
            makeVector(67.6740952099, 0.27764621377, 0.207982882857, 7.39317187957),
            makeVector(50.138641789, 1.05833542347, -1.39942038059, 7.49833627768),
            makeVector(48.1033810447, -0.434369504452, -3.10234260559, 10.5761040522)
        };
        std::vector<double> jetCSVs = {
            0.206025674939, 0.431639432907, 0.993503212929, 0.158015355468, 0.999367594719
        };

        TLorentzVector lepton = makeVector(31.532207489, 1.66870832443, 3.08063626289, 0.105700001113);
        TLorentzVector met = makeVector(0.0, 0.0, 0.0, 0.0); // dummy

        // evaluate
        output5 = dnn.evaluate(jets, jetCSVs, lepton, met);
    }

    // model 6j
    std::vector<TLorentzVector> jets = {
        makeVector(380.695447735, -0.047465339303, 0.691576302052, 27.9791732656),
        makeVector(222.463439395, 2.34676098824, -0.159972220659, 17.2672309435),
        makeVector(156.611973328, 0.00898346677423, -1.56607139111, 16.709882838),
        makeVector(151.287791049, 0.717889666557, -2.78183507919, 23.0265841419),
        makeVector(79.5498694582, 1.11723101139, 2.13706660271, 13.9268736849),
        makeVector(55.8923564036, 1.41984450817, -2.57835197449, 10.3102933688)
    };
    std::vector<double> jetCSVs = {
        0.110297240317, 0.40400609374, 0.989295899868, 0.995724141598, 0.905849277973,
        0.533025145531
    };

    TLorentzVector lepton = makeVector(54.7246551514, 0.540384054184, 2.79249024391, 0.105700001116);
    TLorentzVector met = makeVector(0.0, 0.0, 0.0, 0.0); // dummy

    // evaluate
    output6 = dnn.evaluate(jets, jetCSVs, lepton, met);

    // some output
    std::vector<DNNOutput> outputs = { output4, output5, output6 };
    for (size_t i = 0; i < outputs.size(); ++i)
    {
        std::cout << "evaluation for model " << (i + 4) << "j:" << std::endl;
        std::cout << "0 - ttH:   " << std::fixed << outputs[i].ttH() << std::endl;
        std::cout << "1 - ttbb:  " << std::fixed << outputs[i].ttbb() << std::endl;
        std::cout << "2 - ttb:   " << std::fixed << outputs[i].ttb() << std::endl;
        std::cout << "3 - tt2b:  " << std::fixed << outputs[i].tt2b() << std::endl;
        std::cout << "4 - ttcc:  " << std::fixed << outputs[i].ttcc() << std::endl;
        std::cout << "5 - ttlf:  " << std::fixed << outputs[i].ttlf() << std::endl;
        std::cout << "6 - other: " << std::fixed << outputs[i].other() << std::endl;
        std::cout << "most probable class: " << outputs[i].mostProbableClass() << std::endl;
        std::cout << std::endl;
    }

    //validate outputs
    std::vector<double> targetOutputs4 = { 0.1787547, 0.06417438, 0.1677737, 0.19144671, 0.2523596, 0.14549091, 0.0 };
    std::vector<double> targetOutputs5 = { 0.05385724, 0.10031425, 0.26318933, 0.17002094, 0.24102974, 0.1715885, 0.0 };
    std::vector<double> targetOutputs6 = { 0.27383254, 0.21462227, 0.13850293, 0.24772942, 0.08776784, 0.037545, 0.0 };

    for (size_t i = 0; i < targetOutputs4.size(); ++i)
    {
        assert(fabs(targetOutputs4[i] - output4.values[i]) < EPSILON && "The DNN output for 4 jet events is incorrect");
    }
    for (size_t i = 0; i < targetOutputs5.size(); ++i)
    {
        assert(fabs(targetOutputs5[i] - output5.values[i]) < EPSILON && "The DNN output for 5 jet events is incorrect");
    }
    for (size_t i = 0; i < targetOutputs6.size(); ++i)
    {
        assert(fabs(targetOutputs6[i] - output6.values[i]) < EPSILON && "The DNN output for 6 jet events is incorrect");
    }

    // performance test
    std::cout << "performance test:" << std::endl;
    struct timeval tp;
    gettimeofday(&tp, NULL);
    long int t0 = tp.tv_sec * 1000 + tp.tv_usec / 1000;
    size_t n = 10000;
    for (size_t i = 0; i < n; ++i)
    {
        dnn.evaluate(jets, jetCSVs, lepton, met);
    }
    gettimeofday(&tp, NULL);
    long int t1 = tp.tv_sec * 1000 + tp.tv_usec / 1000;
    int diff = t1 - t0;
    std::cout << n << " evalutions took " << (diff / 1000.) << " seconds" << std::endl;
    std::cout << "=> " << (((float)diff) / n) << " ms per evaluation" << std::endl;

    std::cout << std::endl << "done" << std::endl;
    std::cout << "----------------------------------------" << std::endl << std::endl;

    return 0;
}

int test_DNN_SL_v4()
{
    std::cout << "test DNN SL v4" << std::endl;
    std::cout << "----------------------------------------" << std::endl << std::endl;

    // set precision of numbers in cout
    std::cout.precision(8);
    // setup the dnn classifier
    DNNClassifier_SL dnn("v4");

    DNNOutput output4;
    DNNOutput output5;
    DNNOutput output6;

    // model 4j
    {
        std::vector<TLorentzVector> jets = {
            makeVector(140.75132439, 1.01273322105, -0.134643256664, 17.8163879893),
            makeVector(126.849097007, 1.51873040199, -1.80969059467, 17.1099203825),
            makeVector(65.2795247665, 1.38442242146, -2.99262857437, 11.0165326287),
            makeVector(42.4769117397, -0.271016001701, 0.945309460163, 7.48196756333)
        };
        std::vector<double> jetCSVs = { 0.194645509124, 0.609606564045, 0.868200719357, 0.954067349434 };

        TLorentzVector lepton = makeVector( 58.8885055017, 0.657305300236, 1.42083489895, 0.0 );
        TLorentzVector met = makeVector(0., 0., 0., 0.); //dummy, not needed in this model

        // evaluate
        output4 = dnn.evaluate(jets, jetCSVs, lepton, met);
    }

    // model 5j
    {
        std::vector<TLorentzVector> jets = {
            makeVector(84.2347206168, -0.839125931263, 0.751229822636, 11.6880784858),
            makeVector(82.4362239096, -0.17182123661, 1.359380126, 14.8053830307),
            makeVector(67.097009765, -0.947745680809, -0.680769205093, 13.6073841364),
            makeVector(48.0650410856, 0.582374215126, 0.0201984290034, 8.30356383321),
            makeVector(43.6446700367, -0.572448432446, 2.72874093056, 7.59462365693)
        };
        std::vector<double> jetCSVs = {
            0.99718105793, 0.905391037464, 0.109263792634, 0.427017599344, 0.905423343182
        };

        TLorentzVector lepton = makeVector(155.350942261, 0.15509968996, -2.64386057854, 0.0);
        TLorentzVector met = makeVector(0.0, 0.0, 0.0, 0.0); // dummy

        // evaluate
        output5 = dnn.evaluate(jets, jetCSVs, lepton, met);
    }

    // model 6j
    std::vector<TLorentzVector> jets = {
        makeVector(104.253659103, -0.73279517889, -3.07644724846, 15.8214708715),
        makeVector(93.7207496495, -1.09075820446, -0.518474698067, 11.1629637015),
        makeVector(82.3087599786, -0.29058226943, 0.934316039085, 12.3491756063),
        makeVector(71.0101406197, -0.311807841063, -0.00518677430227, 17.680727362),
        makeVector(48.2224599416, -0.770735502243, 0.56352609396, 7.37671529065),
        makeVector(46.0958273368, -0.883650183678, 1.04447424412, 5.60050991848)
    };
    std::vector<double> jetCSVs = {
        0.597419083118, 0.759489059448, 0.635843455791, 0.603073060513, 0.999078631401, 0.988624215126
    };

    TLorentzVector lepton = makeVector(54.6405308843, -1.68255746365, -2.53192830086, 0.0313574418471);
    TLorentzVector met = makeVector(0.0, 0.0, 0.0, 0.0); // dummy

    // evaluate
    output6 = dnn.evaluate(jets, jetCSVs, lepton, met);

    // some output
    std::vector<DNNOutput> outputs = { output4, output5, output6 };
    for (size_t i = 0; i < outputs.size(); ++i)
    {
        std::cout << "evaluation for model " << (i + 4) << "j:" << std::endl;
        std::cout << "0 - ttH:   " << std::fixed << outputs[i].ttH() << std::endl;
        std::cout << "1 - ttbb:  " << std::fixed << outputs[i].ttbb() << std::endl;
        std::cout << "2 - ttb:   " << std::fixed << outputs[i].ttb() << std::endl;
        std::cout << "3 - tt2b:  " << std::fixed << outputs[i].tt2b() << std::endl;
        std::cout << "4 - ttcc:  " << std::fixed << outputs[i].ttcc() << std::endl;
        std::cout << "5 - ttlf:  " << std::fixed << outputs[i].ttlf() << std::endl;
        std::cout << "6 - other: " << std::fixed << outputs[i].other() << std::endl;
        std::cout << "most probable class: " << outputs[i].mostProbableClass() << std::endl;
        std::cout << std::endl;
    }

    // validate outputs
    std::vector<double> targetOutputs4 = { 0.07752261, 0.10551349, 0.18064148, 0.10610541, 0.31309252, 0.2171245, 0.0 };
    std::vector<double> targetOutputs5 = { 0.26298738, 0.10026517, 0.13860427, 0.11055057, 0.16306332, 0.2245293, 0.0 };
    std::vector<double> targetOutputs6 = { 0.51680371, 0.25959021, 0.07142095, 0.07112622, 0.05624627, 0.02481263, 0.0};

    for (size_t i = 0; i < targetOutputs4.size(); ++i)
    {
        assert(fabs(targetOutputs4[i] - output4.values[i]) < EPSILON && "The DNN output for 4 jet events is incorrect");
    }
    for (size_t i = 0; i < targetOutputs5.size(); ++i)
    {
        assert(fabs(targetOutputs5[i] - output5.values[i]) < EPSILON && "The DNN output for 5 jet events is incorrect");
    }
    for (size_t i = 0; i < targetOutputs6.size(); ++i)
    {
        assert(fabs(targetOutputs6[i] - output6.values[i]) < EPSILON && "The DNN output for 6 jet events is incorrect");
    }

    // performance test
    std::cout << "performance test:" << std::endl;
    struct timeval tp;
    gettimeofday(&tp, NULL);
    long int t0 = tp.tv_sec * 1000 + tp.tv_usec / 1000;
    size_t n = 10000;
    for (size_t i = 0; i < n; ++i)
    {
        dnn.evaluate(jets, jetCSVs, lepton, met);
    }
    gettimeofday(&tp, NULL);
    long int t1 = tp.tv_sec * 1000 + tp.tv_usec / 1000;
    int diff = t1 - t0;
    std::cout << n << " evalutions took " << (diff / 1000.) << " seconds" << std::endl;
    std::cout << "=> " << (((float)diff) / n) << " ms per evaluation" << std::endl;

    std::cout << std::endl << "done" << std::endl;
    std::cout << "----------------------------------------" << std::endl << std::endl;

    return 0;
}

#undef EPSILON
