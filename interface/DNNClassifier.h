/*
 * DNN Classifier.
 * Please note that this classifier actually outputs 7 discriminator values simultaneously.They can
 * be interpreted as a classification probability as they sum up to 1. Classes (order is important):
 * ttH, ttbb, ttb, tt2b, ttcc, ttlf, other
 */

#ifndef TTH_DNNCLASSIFIER_H
#define TTH_DNNCLASSIFIER_H

#include <algorithm>
#include <iterator>
#include <map>
#include <vector>

#include "Python.h"

#include "TLorentzVector.h"
#include "TMatrixDSym.h"
#include "TMatrixDSymEigen.h"
#include "TVectorD.h"

#include "TTH/CommonClassifier/interface/CommonBDTvars.h"

class DNNOutput
{
public:
    std::vector<double> values;

    DNNOutput(double ttH, double ttbb, double ttb, double tt2b, double ttcc, double ttlf,
        double other)
    {
        values.push_back(ttH);
        values.push_back(ttbb);
        values.push_back(ttb);
        values.push_back(tt2b);
        values.push_back(ttcc);
        values.push_back(ttlf);
        values.push_back(other);
    }

    DNNOutput()
        : DNNOutput(0., 0., 0., 0., 0., 0., 0.)
    {
        reset();
    }

    ~DNNOutput()
    {
    }

    void reset();

    inline double ttH() const
    {
        return values[0];
    }

    inline double ttbb() const
    {
        return values[1];
    }

    inline double ttb() const
    {
        return values[2];
    }

    inline double tt2b() const
    {
        return values[3];
    }

    inline double ttcc() const
    {
        return values[4];
    }

    inline double ttlf() const
    {
        return values[5];
    }

    inline double other() const
    {
        return values[6];
    }

    inline size_t mostProbableClass() const
    {
        return distance(values.begin(), max_element(values.begin(), values.end()));
    }
};

class DNNVariables
{
public:
    double getHt(const std::vector<const TLorentzVector*>& lvecs) const;

    void getMinMaxDR(const std::vector<const TLorentzVector*>& lvecs, double& minDR,
        double& maxDR) const;

    void getMinMaxDR(const std::vector<const TLorentzVector*>& lvecs, const TLorentzVector* lvec,
        double& minDR, double& maxDR) const;

    double getCentrality(const std::vector<const TLorentzVector*>& lvecs) const;

    void getSphericalEigenValues(const std::vector<const TLorentzVector*>& lvecs, double& ev1,
        double& ev2, double& ev3) const;
};

class DNNClassifierBase
{
public:
    double csvCut;

    DNNClassifierBase(std::string version);

    virtual ~DNNClassifierBase();

    static void pyInitialize();
    static void pyFinalize();
    static void pyExcept(PyObject* pyObj, const std::string& msg);

protected:
    string version_;
    string inputName_;
    string outputName_;
    string dropoutName_;
    PyObject* pyContext_;
    PyObject* pyEval_;
    DNNVariables dnnVars_;
};

class DNNClassifier_SL : public DNNClassifierBase
{
public:
    DNNClassifier_SL(std::string version = "v4");

    ~DNNClassifier_SL();

    void evaluate(const std::vector<TLorentzVector>& jets, const std::vector<double>& jetCSVs,
        const TLorentzVector& lepton, const TLorentzVector& met, DNNOutput& dnnOutput);

    DNNOutput evaluate(const std::vector<TLorentzVector>& jets, const std::vector<double>& jetCSVs,
        const TLorentzVector& lepton, const TLorentzVector& met);

    void fillFeatures_(PyObject* pyEvalArgs, const std::vector<TLorentzVector>& jets,
        const std::vector<double>& jetCSVs, const TLorentzVector& lepton,
        const TLorentzVector& met);

private:
    size_t nFeatures4_;
    size_t nFeatures5_;
    size_t nFeatures6_;
    PyObject* pyEvalArgs4_;
    PyObject* pyEvalArgs5_;
    PyObject* pyEvalArgs6_;
    CommonBDTvars bdtVars_;
};

class DNNClassifier_DL : public DNNClassifierBase
{
public:
    DNNClassifier_DL(std::string version = "v1");

    ~DNNClassifier_DL();

    void evaluate(const std::vector<TLorentzVector>& jets, const std::vector<double>& jetCSVs,
        const std::vector<TLorentzVector>& leptons, const TLorentzVector& met,
        DNNOutput& dnnOutput);

    DNNOutput evaluate(const std::vector<TLorentzVector>& jets, const std::vector<double>& jetCSVs,
        const std::vector<TLorentzVector>& leptons, const TLorentzVector& met);

    void fillFeatures_(PyObject* pyEvalArgs, const std::vector<TLorentzVector>& jets,
        const std::vector<double>& jetCSVs, const std::vector<TLorentzVector>& leptons,
        const TLorentzVector& met);

private:
    size_t nFeatures3_;
    size_t nFeatures4_;
    PyObject* pyEvalArgs3_;
    PyObject* pyEvalArgs4_;
};

// python evaluation script
static string evalScript = "\
import sys, numpy as np\n\
td, inputs, outputs, dropouts = None, [], [], []\n\
def setup(python_path, model_files, input_name, output_name, dropout_name):\n\
    global td, inputs, outputs, dropouts\n\
    sys.path.insert(0, python_path)\n\
    import tfdeploy as td\n\
    for model_file in model_files:\n\
        model = td.Model(model_file)\n\
        inputs.append(model.get(input_name))\n\
        outputs.append(model.get(output_name))\n\
        dropouts.append(model.get(dropout_name))\n\
def eval(m, *values):\n\
    return list(outputs[m].eval({inputs[m]: [np.array(values).astype(np.float32)], dropouts[m]: 1.})[0])\n\
";

#endif // TTH_DNNCLASSIFIER_H
