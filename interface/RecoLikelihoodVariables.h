#ifndef _RecoLikelihoodVariables_h
#define _RecoLikelihoodVariables_h

#include <vector>
#include <map>

#include "TVector.h"
#include "TLorentzVector.h"

#include "TTH/RecoLikelihoodReconstruction/interface/ReconstructionQuality.hpp"
#include "TTH/RecoLikelihoodReconstruction/interface/InterpretationGenerator.hpp"
#include "TTH/RecoLikelihoodReconstruction/interface/Interpretation.hpp"


class RecoLikelihoodVariables{
    
    public:
        RecoLikelihoodVariables();
    
        void CalculateRecoLikelihoodVariables(const std::vector<TLorentzVector>& selectedLeptonP4,
                                              const std::vector<TLorentzVector>& selectedJetP4,
                                              const std::vector<double>&         selectedJetCSV,
                                              const TLorentzVector&              metP4);

        std::map<TString,double> GetRecoLikelihoodVariables();
        
    private:

	ReconstructionQuality quality;
	InterpretationGenerator generator;

	std::vector<std::string> tags_tt;
	std::vector<std::string> tags_ttbb;
	std::vector<std::string> tags_tth;
	std::vector<std::string> alltags;
	std::vector<std::string> ratiotags_name;
	std::vector<std::string> ratiotags_tth;
	std::vector<std::string> ratiotags_ttbb;


	double m_Reco_Dr_BB_best_TTLikelihood;
	double m_Reco_Dr_BB_best_TTLikelihood_comb;

	double m_Reco_Higgs_M_best_TTLikelihood;
	double m_Reco_Higgs_M_best_TTLikelihood_comb;

	double m_Reco_TTHLikelihood_best_TTLikelihood;
	double m_Reco_TTHLikelihood_best_TTLikelihood_comb;

	double m_Reco_TTHBBME_best_TTLikelihood;
	double m_Reco_TTHBBME_best_TTLikelihood_comb;

	double m_Reco_TTHLikelihoodTimesME_best_TTLikelihood;
	double m_Reco_TTHLikelihoodTimesME_best_TTLikelihood_comb;

	double m_Reco_TTBBLikelihood_best_TTLikelihood;
	double m_Reco_TTBBLikelihood_best_TTLikelihood_comb;

	double m_Reco_TTBBME_best_TTLikelihood;
	double m_Reco_TTBBME_best_TTLikelihood_comb;

	double m_Reco_TTBBME_off_best_TTLikelihood;
	double m_Reco_TTBBME_off_best_TTLikelihood_comb;

	double m_Reco_TTBBLikelihoodTimesME_best_TTLikelihood;
	double m_Reco_TTBBLikelihoodTimesME_best_TTLikelihood_comb;

	double m_Reco_TTBBLikelihoodTimesME_off_best_TTLikelihood;
	double m_Reco_TTBBLikelihoodTimesME_off_best_TTLikelihood_comb;

	double m_Reco_LikelihoodRatio_best_TTLikelihood;
	double m_Reco_LikelihoodRatio_best_TTLikelihood_comb;

	double m_Reco_MERatio_best_TTLikelihood;
	double m_Reco_MERatio_best_TTLikelihood_comb;

	double m_Reco_MERatio_off_best_TTLikelihood;
	double m_Reco_MERatio_off_best_TTLikelihood_comb;

	double m_Reco_LikelihoodTimesMERatio_best_TTLikelihood;
	double m_Reco_LikelihoodTimesMERatio_best_TTLikelihood_comb;

	double m_Reco_LikelihoodTimesMERatio_off_best_TTLikelihood;
	double m_Reco_LikelihoodTimesMERatio_off_best_TTLikelihood_comb;

	double m_Reco_TTHLikelihood_best_TTHLikelihood;
	double m_Reco_TTHLikelihood_best_TTHLikelihoodTimesME;

	double m_Reco_TTHBBME_best_TTHLikelihood;
	double m_Reco_TTHBBME_best_TTHLikelihoodTimesME;

	double m_Reco_TTHLikelihoodTimesME_best_TTHLikelihood;
	double m_Reco_TTHLikelihoodTimesME_best_TTHLikelihoodTimesME;

	double m_Reco_TTBBLikelihood_best_TTBBLikelihood;
	double m_Reco_TTBBLikelihood_best_TTBBLikelihoodTimesME;

	double m_Reco_TTBBME_best_TTBBLikelihood;
	double m_Reco_TTBBME_best_TTBBLikelihoodTimesME;

	double m_Reco_TTBBLikelihoodTimesME_best_TTBBLikelihood;
	double m_Reco_TTBBLikelihoodTimesME_best_TTBBLikelihoodTimesME;

	double m_Reco_TTBBME_off_best_TTBBLikelihood;
	double m_Reco_TTBBME_off_best_TTBBLikelihoodTimesME;

	double m_Reco_TTBBLikelihoodTimesME_off_best_TTBBLikelihood;
	double m_Reco_TTBBLikelihoodTimesME_off_best_TTBBLikelihoodTimesME;

	double m_Reco_Deta_Fn_best_TTBBLikelihood;
	double m_Reco_Deta_Fn_best_TTBBLikelihoodTimesME;

	double m_Reco_Deta_TopLep_BB_best_TTBBLikelihood;
	double m_Reco_Deta_TopLep_BB_best_TTBBLikelihoodTimesME;

	double m_Reco_Deta_TopHad_BB_best_TTBBLikelihood;
	double m_Reco_Deta_TopHad_BB_best_TTBBLikelihoodTimesME;

	double m_Reco_LikelihoodRatio_best_Likelihood;
	double m_Reco_LikelihoodRatio_best_LikelihoodTimesME;

	double m_Reco_MERatio_best_Likelihood;
	double m_Reco_MERatio_best_LikelihoodTimesME;

	double m_Reco_MERatio_off_best_Likelihood;
	double m_Reco_MERatio_off_best_LikelihoodTimesME;

	double m_Reco_LikelihoodTimesMERatio_best_Likelihood;
	double m_Reco_LikelihoodTimesMERatio_best_LikelihoodTimesME;

	double m_Reco_LikelihoodTimesMERatio_off_best_Likelihood;
	double m_Reco_LikelihoodTimesMERatio_off_best_LikelihoodTimesME;

	double m_Reco_Sum_TTHLikelihood;
	double m_Reco_Sum_TTBBLikelihood;
	double m_Reco_Sum_TTHLikelihoodTimesME;
	double m_Reco_Sum_TTBBLikelihoodTimesME;
    	double m_Reco_Sum_TTHBBME;
	double m_Reco_Sum_TTBBME;
	double m_Reco_Sum_LikelihoodRatio;
	double m_Reco_Sum_LikelihoodTimesMERatio;
	double m_Reco_Sum_MERatio;	


        double CSVLwp, CSVMwp, CSVTwp;
 
};

#endif
